package FipkartTestScripts;
//import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginModalValidAcctIncorrectPass {
	String baseUrl = "";
	WebDriver driver;
		
	public LoginModalValidAcctIncorrectPass(String currentUrl, WebDriver getDriver) {
		this.baseUrl = currentUrl;
		this.driver = getDriver;
	}
	
	public void loginModalValidAcctIncorrectPass() {
		try {					
			driver.get(baseUrl);
			
			WebElement userLogin = driver.findElement(By.cssSelector("#fk-mainhead-id .header-links ul > li > .login-required"));
		       userLogin.click();
		       
		       WebDriverWait wait = new WebDriverWait(driver, 10);
		       
		       boolean isLoginSuccessful = false;
		       
		       if(driver.findElements(By.id("login-signup-newDialog")).size() !=0) {
			       WebElement loginName = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login-signup-newDialog .user-email")));               
			       loginName.sendKeys("9987174486");
			       
			       WebElement loginPassword = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login-signup-newDialog .user-pwd")));
			       loginPassword.sendKeys("Incorrect");
			       
			       WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login-signup-newDialog .login-btn")));
			       loginButton.click();
			       isLoginSuccessful = true;
			   }
		       
		       System.out.println(isLoginSuccessful);
			
		} catch(Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
	}
	
}
