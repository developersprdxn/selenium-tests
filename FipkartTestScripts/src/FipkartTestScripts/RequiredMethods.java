package FipkartTestScripts;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RequiredMethods {
	
	public boolean isElementPresent(String element, String method, WebDriver driver) {
		boolean status; 
		switch (method) {
		case "className":
			status = (driver.findElements(By.className(element)).size() != 0) ? true : false;
			break;
		
		case "id":
			status = (driver.findElements(By.id(element)).size() != 0) ? true : false;
			break;
			
		case "xpath":
			status = (driver.findElements(By.xpath(element)).size() != 0) ? true : false;
			break;
			
		case "cssSelector":
			status = (driver.findElements(By.cssSelector(element)).size() != 0) ? true : false;
			break;

		default:
			status = false;
			break;
		}
		return 	status;	
	}

	public int getRandomNum(int min, int max) {
	    Random rand = new Random();
	    
//		Generate random number
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	
	public int countElements(String element, String method, WebDriver driver) {
		int count; 
		switch (method) {
		case "className":
			count = driver.findElements(By.className(element)).size();
			break;
		
		case "id":
			count = driver.findElements(By.id(element)).size();
			break;
			
		case "xpath":
			count = driver.findElements(By.xpath(element)).size();
			break;
			
		case "cssSelector":
			count = driver.findElements(By.cssSelector(element)).size();
			break;

		default:
			count = 0;
			break;
		}
		return 	count;
	}
	
	public WebElement getElement(String element, String method, WebDriver driver) {
		WebElement tempElement;
		switch (method) {
		case "className":
			tempElement = driver.findElement(By.className(element));
			break;
		
		case "id":
			tempElement = driver.findElement(By.id(element));
			break;
			
		case "xpath":
			tempElement = driver.findElement(By.xpath(element));
			break;
			
		case "cssSelector":
			tempElement = driver.findElement(By.cssSelector(element));
			break;

		default:
			tempElement = null;
			break;
		}
		return 	tempElement;
	}
	
	public void changeSliderElement(String button, int increment, int current, String method, WebDriver driver) throws InterruptedException {
		int limit = current / increment;
		for (int i = 1; i <= limit; i++) {
			System.out.println("Click "+i);
			switch (method) {
			case "className":
				new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.className(button))).click();
				break;
			
			case "id":
				new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.id(button))).click();
				break;
				
			case "xpath":
				new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(button))).click();
				break;
				
			case "cssSelector":
				new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(button))).click();
				break;

			default:
				break;
			}
			Thread.sleep(500);			
		}
		
		
		
	}

	public WebDriver getFireFoxDriver(String profileName) {
//		Set Profile for browser
		ProfilesIni allprofile = new ProfilesIni();
		FirefoxProfile profile = allprofile.getProfile(profileName);
		
//		Redirect to url
		return new FirefoxDriver(profile);
	}
}
