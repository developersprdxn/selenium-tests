package FipkartTestScripts;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

public class RemoveItemFromCart {
	
	String currentUrl = "";
	int itemToRemove = 0;
	WebDriver driver= null;
	
	//User Inputs
	String btnMyCart = ".btn-cart"; //<-- Add CSS Selector of My Cart Button (CssSelector of Cart Button)
	String productList = ".cart-table"; //<-- Add CSS Selector of parent table of product lists in Cart (CssSelector for parent table list of Products in cart)
	String emptyCart = ".empty-cart-message"; //<-- Add CSS Selector of Empty Cart 
	
	public RemoveItemFromCart(String getUrl, int getNumberOfProducts, WebDriver getDriver) {
		this.currentUrl = getUrl;
		this.itemToRemove = getNumberOfProducts;
		this.driver = getDriver;
	}
	
  public void RemoveItem() {
	  try {
            //Set Profile for browser
//			ProfilesIni allprofile = new ProfilesIni();
//			FirefoxProfile profile = allprofile.getProfile("default");
			
			//Create a new instance of the Firefox driver
//		    WebDriver driver = new FirefoxDriver(profile);
		       
		    //Maximize the window
		    //driver.manage().window().maximize();
		    
		    driver.get(currentUrl);
		 
//			UserLogin login = new UserLogin();
//			if(login.UserLogin(driver)) {
//		    	   System.out.println("PASS --> SUCCESSFULLY LOGGED-IN");
//		    	   assertTrue("PASS ---> USER LOGGED-IN SUCCESSFULLY", true);
//			}
//			else {
//		    	   System.out.println("FAIL --> LOGIN FAILURE");
//			}
//			
//			Thread.sleep(1000);
			
			WebElement myCartButton = driver.findElement(By.cssSelector(btnMyCart));
			myCartButton.click();
			
//			Object of RequiredMethods class
			RequiredMethods getMethod = new RequiredMethods();
			
			if(getMethod.isElementPresent(emptyCart, "cssSelector", driver)) {
				System.out.println(driver.findElement(By.cssSelector(emptyCart)).getText());
			}
			else {
				//WebElement productTableList = driver.findElement(By.cssSelector(productList));
				boolean isRemove = ItemRemove(productList, itemToRemove, driver);
				if(isRemove) {
					System.out.println("PASS --> PRODUCTS SUCCESSFULLY REMOVED");
				}
				else {
					System.err.println("FAIL --> PRODUCTS NOT REMOVED");
					assertTrue("FAIL ---> THIS TEST CASE FAILED DUE TO PRODUCTS NOT REMOVED", false);
				}
			}
			//driver.quit();
		   }
		   catch(Exception e) {
			 System.err.println("Error: " + e.getMessage());
			 System.err.println("\n\n--------This test case fail------------\n\n");
		     assertTrue("FAIL ---> THIS TEST CASE FAILED DUE TO EXCEPTION - " +e.getMessage(), false);
		   }
  }
  
  public boolean ItemRemove(String parentTablePath, int numberOfItemsToRemove, WebDriver driver) throws InterruptedException {
	  //Path to get Total number of rows of Product in the list
	  int totalProducts = driver.findElements(By.cssSelector(parentTablePath+ " > tbody > tr")).size();
	  WebElement productRow = null;
	  boolean isItemRemove = false;
	  
	  if(totalProducts >= numberOfItemsToRemove) {
		  for(int i=1; i<=numberOfItemsToRemove; i++) {
			  //Give a Path of Remove Product link
			  productRow = driver.findElement(By.cssSelector(parentTablePath+ " > tbody > tr:first-child .cart-remove-item"));
			  
			  //CssSelector Path to Show title of Product in the list
			  System.out.println(driver.findElement(By.cssSelector(parentTablePath+ " > tbody > tr:first-child .title")).getText() + " Removed");
			  productRow.click();
			  
			  Thread.sleep(5000);
		  }
		  isItemRemove = true;  
	  }
	  else {
		  System.err.println("Please Enter Valid Number of Products to Remove From Cart - The Total Number of Products in Cart is Less");
		  isItemRemove = false;  
	  }
	  
	  return isItemRemove;
  }
  
  public void OnHover(WebElement currentElement, WebDriver driver) {
	    Actions builder = new Actions(driver); 
		Actions hoverOverMenuCategory = builder.moveToElement(currentElement);
		hoverOverMenuCategory.perform();
  }
}
