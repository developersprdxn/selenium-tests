package FipkartTestScripts;

//import org.junit.Assert;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class cartCheckWithItems {
	String baseUrl = "";
	WebDriver driver;
		 
	
	public cartCheckWithItems(String currentUrl, WebDriver getDriver) {
		this.baseUrl = currentUrl;
		this.driver = getDriver;
	}
	
	
	public void cartCheckWithItems() {
		try {		
		RequiredMethods getMethod = new RequiredMethods();		
		//WebDriver driver = getMethod.getFireFoxDriver("default");
		driver.get(baseUrl);
		
		//Get Count of Current Total Items in Cart
		WebElement btncart = getMethod.getElement("item_count_in_cart_top_displayed", "id", driver);
		int countTotalCartItems = Integer.parseInt(btncart.getText());
		
		//Click on the Cart button (Which will re-direct to the Cart page)
		btncart.click();
		
		//Compare Current count of Items with that of the ones on Cart Page
		int cartPageItemCount = getMethod.countElements("#cartpage-cart-tab-content .cart-body > tr", "cssSelector", driver);
		if(cartPageItemCount == countTotalCartItems) {
			System.out.println("Items added to cart are present in cart page");
			}
		assertTrue("FAIL ---> ", cartPageItemCount == countTotalCartItems);
		
		} catch(Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
		
	}
	
}
