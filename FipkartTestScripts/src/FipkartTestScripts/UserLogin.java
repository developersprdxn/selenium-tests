package FipkartTestScripts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
public class UserLogin {
		public boolean UserLoginFun(WebDriver driver) {
			boolean isLoginSuccessful = false;
			try {
				   String loginUrl = "http://www.flipkart.com/";				   
				   driver.get(loginUrl);
				   
			       WebElement userLogin = driver.findElement(By.cssSelector("#fk-mainhead-id .header-links ul > li > .login-required"));
			       userLogin.click();
			       
			       WebDriverWait wait = new WebDriverWait(driver, 10);
			       
			       if(driver.findElements(By.id("login-signup-newDialog")).size() !=0) {
				       WebElement loginName = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login-signup-newDialog .user-email")));               
				       loginName.sendKeys("9987174486");
				       
				       WebElement loginPassword = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login-signup-newDialog .user-pwd")));
				       loginPassword.sendKeys("Reena@2o!5");
				       
				       WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login-signup-newDialog .login-btn")));
				       loginButton.click();
				       isLoginSuccessful = true;
				   }
			       
			} catch(Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
			return isLoginSuccessful;
		}

}
