package FipkartTestScripts;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class LogInCartClickWithItems {
	String baseUrl = "";
	
	public LogInCartClickWithItems(String currentUrl) {
		this.baseUrl = currentUrl;
	}
	
	public void logInCartClickWithItems() {
		try {
			RequiredMethods getMethod = new RequiredMethods();		
			WebDriver driver = getMethod.getFireFoxDriver("default");
			driver.get(baseUrl);
			
			UserLogin login = new UserLogin();
			if(login.UserLoginFun(driver)) {
		    	   System.out.println("PASS --> SUCCESSFULLY LOGGED-IN");
		    	   assertTrue("PASS ---> USER LOGGED-IN SUCCESSFULLY", true);
			}
			else {
		    	   System.out.println("FAIL --> LOGIN FAILURE");
			}			
			Thread.sleep(1000);		
			
			//Get Count of Current Total Items in Cart
			WebElement btncart = getMethod.getElement("item_count_in_cart_top_displayed", "id", driver);
			int countTotalCartItems = Integer.parseInt(btncart.getText());
			
			//Click on the Cart button (Which will re-direct to the Cart page)
			btncart.click();
			
			//Compare Current count of Items with that of the ones on Cart Page
			int cartPageItemCount = getMethod.countElements("#cartpage-cart-tab-content .cart-body > tr", "cssSelector", driver);
			if(cartPageItemCount == countTotalCartItems) {
				System.out.println("Items added to cart are present in cart page");
			}
			assertTrue("FAIL ---> ", cartPageItemCount == countTotalCartItems);
			driver.quit();
		} catch(Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
		
	}	

}
