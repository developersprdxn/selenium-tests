package FipkartTestScripts;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddToCart {
	
//	Initial required information - No need to initialize
	String url;
	String productsPath[];
	int productToBeAdd;
	boolean isManual = false;
	WebDriver driver;
	
//	Some required path

//	If you are doing changes in path then adjust corresponding methods to accesses element
	
//	Set cart path
	String cartNumberPath = "item_count_in_cart_top_displayed";
//	Set add to cart button path
	String addToCartButtonPath = "btn-buy-now";
//	Set path of product row
	String productRowPath = "#products .browse-grid-row";
//	Set thumbnail/saction path
	String thumbnailPath = ".multiSelectionWrapper .multiSelectionWidget + .multiSelectionWidget .multiSelectionWidget-selectors-wrap";
//	set thumbnail/saction number - it should not be 0
	int thumbnailNumber = 1;
	
//	constructor to set url, number of products to be add and path of product if any
	public AddToCart(String url, int productToBeAdd, String productsPath[], WebDriver getDriver) {
		this.url = url;
		this.productToBeAdd = productToBeAdd;
		this.productsPath = productsPath;
		this.driver = getDriver;
		if(this.productsPath.length > 0) {
			this.isManual = true;
		}
	}
	
	public void addToCartFun() {
		try {
			
			RequiredMethods getMethod = new RequiredMethods();
			
			 driver.manage().window().maximize();
			
//			Object to wait for element
			WebDriverWait wait = new WebDriverWait(driver, 15);
			
			int i = 0;
			while(i < productToBeAdd) {
				System.out.println("PRODUCT "+(i+1));
				
//				Redirect to url
				driver.get(url);
				
				if(!isManual) {
					
//					Specify selector for row of product listing page (From where need to select product)
					int randomProductRow = getMethod.getRandomNum(1, getMethod.countElements(productRowPath, "cssSelector", driver));
					System.out.println(randomProductRow+" ROW OF PRODUCT SELECTED");

//					Specify selector for product from selected row
//					Update this code as required
					int randomProduct = getMethod.getRandomNum(1, getMethod.countElements("//*[@id='products']/div/div[contains(@class, 'browse-grid-row')][position() ="+randomProductRow+"]/div[contains(@class, 'unit')]", "xpath", driver));

					if(getMethod.isElementPresent("//*[@id='products']/div/div[contains(@class, 'browse-grid-row')][position() ="+randomProductRow+"]/div["+randomProduct+"]/div/div/a", "xpath", driver)) {
						driver.findElement(By.xpath("//*[@id='products']/div/div[contains(@class, 'browse-grid-row')][position() ="+randomProductRow+"]/div["+randomProduct+"]/div/div/a")).click();
						System.out.println("PASS ---> "+randomProduct+" PRODUCT HAS SELECTED");
					} else {
						System.err.println("FAIL ---> FAIL TO FIND "+randomProduct+" PRODUCT");
					}
					
				} else {
					
					if(getMethod.isElementPresent(productsPath[i], "xpath", driver)) {
						driver.findElement(By.xpath(productsPath[i])).click();
						System.out.println("PASS ---> "+(i+1)+" PRODUCT HAS SELECTED");
					} else {
						System.err.println("FAIL ---> FAIL TO FIND "+(i+1)+" PRODUCT");
					}
					
				}
				
				int currentCartCount = Integer.parseInt(driver.findElement(By.id(cartNumberPath)).getText()); 
				
				
//				click on add to cart button and get cart count
				int cartCountAfterClick = clickAddToCart(driver);
				
//				Check whether cart count incressed or not after clicked on add to cart button 
				if(cartCountAfterClick == currentCartCount) {
					System.out.println("NEED CONFUGRATION");
					setProductConfig(driver);
					cartCountAfterClick = clickAddToCart(driver);
				}
				
				System.out.println("CART COUNT BEFORE CLICK "+currentCartCount+" CART COUNT AFTER CLICK "+cartCountAfterClick);
				
				if(cartCountAfterClick != currentCartCount) {
					System.out.println("PASS ---> PRODUCT HAS ADDED IN CART");
				} else {
					System.err.println("FAIL ---> FAIL TO ADD PRODUCT IN CART");
				}
				
//				counter for products
				i++;
				
//				Wait for moment
				Thread.sleep(3000);
				
				System.out.println("\n-----------------------------\n");
			}
			
			System.out.println("\n\n\n---END---");
						
			
		} catch (Exception e) {
			System.err.println("Exception ocuured -> "+e.getMessage());
			System.err.println("\n\n--------This test case fail------------\n\n");
			assertTrue("FAIL ---> THIS TEST CASE FAILED DUE TO EXCEPTION - "+e.getMessage(), false);
		}
	}
	
	public int clickAddToCart(WebDriver driver) throws InterruptedException {
		if(new RequiredMethods().isElementPresent(addToCartButtonPath, "className", driver)) {
			driver.findElement(By.className(addToCartButtonPath)).click();
			Thread.sleep(2000);
		}
		return Integer.parseInt(driver.findElement(By.id(cartNumberPath)).getText());
	}
	
	public void setProductConfig(WebDriver driver) {		
//		Click on thumbnail/saction
		if(new RequiredMethods().isElementPresent(thumbnailPath+" a:nth-child("+thumbnailNumber+")", "cssSelector", driver)) {
			driver.findElement(By.cssSelector(thumbnailPath+" a:nth-child("+thumbnailNumber+")")).click();
			System.out.println("ANCHOR CLICKED");
		} else if(new RequiredMethods().isElementPresent(thumbnailPath+" div:nth-child("+thumbnailNumber+")", "cssSelector", driver)) {
			driver.findElement(By.cssSelector(thumbnailPath+" div:nth-child("+thumbnailNumber+")")).click();
			System.out.println("DIV CLICKED");
		}
	}
}
