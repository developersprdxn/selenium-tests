package FipkartTestScripts;

//import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class cartCheckWithoutItems {
String baseUrl = "";
		 
	
	public cartCheckWithoutItems(String currentUrl) {
		this.baseUrl = currentUrl;
	}
	
	public void cartCheckWithItem() {
		RequiredMethods getMethod = new RequiredMethods();		
		WebDriver driver = getMethod.getFireFoxDriver("default");
		driver.get(baseUrl);
		
		//Click on the Cart button (Which will re-direct to the Cart page)
		WebElement btncart = getMethod.getElement("item_count_in_cart_top_displayed", "id", driver);
		btncart.click();
		
		//Check whether Cart Page is Empty
		boolean checkStatus = false;
		if(getMethod.isElementPresent("#cartpage-cart-tab-content .empty-cart-message", "cssSelector", driver)) {
			checkStatus = true;
		} else {
			checkStatus = false;
		}
		assertTrue("FAIL ---> ", checkStatus);
	}
	
	
}
