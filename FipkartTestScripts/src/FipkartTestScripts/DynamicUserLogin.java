package FipkartTestScripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

public class DynamicUserLogin {
	String baseUrl = "";
	String userName = "";
	String password = "";
	String errMsgSelector = "#login-signup-newDialog .login_error";
	boolean isLoginSuccessful = false;
	boolean loginFail = false;
	RequiredMethods getMethod = new RequiredMethods();
	WebDriver driver;

	public DynamicUserLogin(String baseUrl, String userName, String password) {
		this.baseUrl = baseUrl;
		this.userName = userName;
		this.password = password;
	}

	public boolean dynamicUserLogin(WebDriver driver) {
		try {
			driver.get(baseUrl);
			WebElement userLogin = driver.findElement(By.cssSelector("#fk-mainhead-id .header-links ul > li > .login-required"));
			userLogin.click();
			WebDriverWait wait = new WebDriverWait(driver, 10);

			if (driver.findElements(By.id("login-signup-newDialog")).size() != 0) {

				WebElement loginName = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login-signup-newDialog .user-email")));
				loginName.sendKeys(userName);

				WebElement loginPassword = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login-signup-newDialog .user-pwd")));
				loginPassword.sendKeys(password);

				WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login-signup-newDialog .login-btn")));
				loginButton.click();
				Thread.sleep(5000);
			}

		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
		return isLoginSuccessful;
	}
	
	public void errorLogin(WebDriver driver) {
		dynamicUserLogin(driver);
		if (getMethod.isElementPresent(errMsgSelector, "cssSelector", driver)) {
			isLoginSuccessful = false;
			loginFail = true;
			WebElement errorMsgContainer = getMethod.getElement(errMsgSelector, "cssSelector", driver);
			String errorMsg = errorMsgContainer.findElement(By.cssSelector(".err_text")).getText();
			System.err.println("Login Failed");
			System.err.print(errorMsg);
		} else {
			isLoginSuccessful = true;
			loginFail = false;
			System.out.print("LOGIN SUCCESSFULL");
		}
		assertTrue("FAIL ---> User has logged in with invalid credenials", loginFail);
	}
}
