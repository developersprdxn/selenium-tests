package FipkartTestScripts;

//import static org.junit.Assert.*;

//import org.junit.After;
//import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class CartModule {

	// Object of RequiredMethods class
	RequiredMethods getMethod = new RequiredMethods();

	// Get firefox driver
	WebDriver driver = getMethod.getFireFoxDriver("default");

	// @Before
	// public void isLoggedIn() {
	// if(new UserLogin().UserLogin()) {
	// System.out.println("PASS --> SUCCESSFULLY LOGGED-IN");
	// assertTrue("PASS ---> USER LOGGED-IN SUCCESSFULLY", true);
	// }
	// else {
	// System.out.println("FAIL --> LOGIN FAILURE");
	// }
	// }

	// @Test
	// public void addToCart() {
	// System.out.println("\n\n-------Add To Cart--------\n\n");
	// String url =
	// "http://www.flipkart.com/mens-clothing/trousers/pr?sid=2oq,s9b,9uj&otracker=clp_mens-clothing_CategoryLinksModule_0-2_catergorylinks_4_Trousers";
	// String productsPath[] = {
	// "//*[@id='products']/div/div[1]/div[2]/div/div[1]/a[1]",
	// "//*[@id='products']/div/div[3]/div[2]/div/div[1]/a[1]",
	// "//*[@id='products']/div[1]/div[7]/div[2]/div/div[1]/a[1]"
	// };
	// new AddToCart(url, 1, productsPath, driver).addToCartFun();
	// }

	// @Test
	// public void cartWithItems() {
	// String baseUrl = "http://www.flipkart.com/";
	// System.out.println("\n\n-------Check Cart With Items--------\n\n");
	// new cartCheckWithItems(baseUrl, driver).cartCheckWithItems();
	// }
	//
	// @After
	// public void removefromCart() {
	// System.out.println("\n\n-------Remove Items From Cart--------\n\n");
	// new RemoveItemFromCart("http://www.flipkart.com/", 1,
	// driver).RemoveItem();
	// }

	// @Test
	// public void cartClickWithItems() {
	// String baseUrl = "http://www.flipkart.com/";
	// System.out.println("\n\n-------Check Cart Page With Items--------\n\n");
	// new CartClickWithItems(baseUrl).cartClickWithItems();
	// }

	// @Test
	// public void loginCartClickWithItems() {
	// String baseUrl = "http://www.flipkart.com/";
	// System.out.println("\n\n-------Check Cart Page With Items--------\n\n");
	// new LogInCartClickWithItems(baseUrl).logInCartClickWithItems();
	// }

	// @Test
	// public void withoutLoginBuyNowClick() {
	// System.out.println("\n\n-------Add To Cart--------\n\n");
	// String url =
	// "http://www.flipkart.com/mens-clothing/trousers/pr?sid=2oq,s9b,9uj&otracker=clp_mens-clothing_CategoryLinksModule_0-2_catergorylinks_4_Trousers";
	// String productsPath[] = {
	// "//*[@id='products']/div/div[1]/div[2]/div/div[1]/a[1]"
	// };
	// new WithoutLoginBuyNow(url, 1, productsPath,
	// driver).withoutLoginbuyNowClick();
	// }

	// @Test
	// public void loginBuyNowClick() {
	// System.out.println("\n\n-------Add To Cart--------\n\n");
	// String url =
	// "http://www.flipkart.com/mens-clothing/trousers/pr?sid=2oq,s9b,9uj&otracker=clp_mens-clothing_CategoryLinksModule_0-2_catergorylinks_4_Trousers";
	// String productsPath[] = {
	// "//*[@id='products']/div/div[1]/div[2]/div/div[1]/a[1]"
	// };
	// new LoginByNow(url, 1, productsPath, driver).loginBuyNowClick();
	// }

	// @Test
	// public void LoginModalValidAcctIncorrectPass() {
	// System.out.println("\n\n-------Check Login with valid uername and invalid password--------\n\n");
	// String username = "9987174486";
	// String password = "Incorrect";
	// String cssSelector = "#login-signup-newDialog .login_error";
	// new DynamicUserLogin(username, password,
	// cssSelector).dynamicUserLogin(driver);
	// }

	// @Test
	// public void LoginModalInvalidAcct() {
	// System.out.println("\n\n-------Check Login with valid uername and invalid password--------\n\n");
	// String username = "Incorrect";
	// String password = "Incorrect";
	// String cssSelector = "#login-signup-newDialog .login_error";
	// new DynamicUserLoginWithError(username, password,
	// cssSelector).dynamicUserLogin(driver);
	// }

//	@Test
//	public void ForgotPasswordInvalidAccount() {
//		System.out.println("\n\n-------Forgot Password with invalid Username/Email--------\n\n");
//		String username = "parag.kamble@prdxn.com";
//		String cssSelector = "#login-signup-newDialog .login_error";
//		new ForgotPasswordInvalidAccount(username, cssSelector).forgotPasswordInvalidAccount(driver);
//	}
	
	@Test
	public void DynamicUserLogin() {
		System.out.println("\n\n-------Valid User Login--------\n\n");
		String baseUrl = "http://www.flipkart.com";
		String username = "tanaya.dipnaik.prdxn@gmail.com";
		String password = "prdxn123";
		new DynamicUserLogin(baseUrl, username, password).errorLogin(driver);;
	}
}
