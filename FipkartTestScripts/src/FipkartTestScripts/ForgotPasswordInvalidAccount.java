package FipkartTestScripts;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ForgotPasswordInvalidAccount {
	String userName = "";
	String cssSelector = "";
	
	public ForgotPasswordInvalidAccount(String userName, String elementLocation) {
		this.userName = userName;
		this.cssSelector = elementLocation;
	}
	public void forgotPasswordInvalidAccount(WebDriver driver) {
		boolean isError;
		RequiredMethods getMethod = new RequiredMethods();
		try {
		   String loginUrl = "http://www.flipkart.com/";
		   
		   driver.get(loginUrl);
		   
	       WebElement userLogin = driver.findElement(By.cssSelector("#fk-mainhead-id .header-links ul > li > .login-required"));
	       userLogin.click();
	       
	       WebDriverWait wait = new WebDriverWait(driver, 10);	       
	       
	       if(driver.findElements(By.id("login-signup-newDialog")).size() !=0) {
		       WebElement loginName = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login-signup-newDialog .user-email")));               
		       loginName.sendKeys(userName);
		       
		       WebElement btnForgotPassword = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#login-signup-newDialog .frgt-pswd")));		       
		       btnForgotPassword.click();
		   }
	       
	       Thread.sleep(10000);
	       
	       WebElement errorMsgContainer = getMethod.getElement(cssSelector, "cssSelector", driver);
	       
	       String errorMsg = errorMsgContainer.findElement(By.cssSelector(".err_text")).getText();
	       
	       if(getMethod.isElementPresent(cssSelector, "cssSelector", driver)) {
	    	   isError = true;
	    	   System.out.println("Error " +errorMsg + " has occured");
	       } else {
	    	   isError = false;
	       }
	       assertTrue("FAIL ---> User has logged in with invalid credenials", isError);
		} catch(Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
	       
	}
}